
	$(document).ready(function() {
		$('#dataTables-example').DataTable();
		$("#dataTables-example").on('click', '#deleteContent', function() {
			var currentRow = $(this).closest("tr");
			var id = currentRow.find("td:eq(0)").text();
			
				$.get({
					url : "deleteContent",
					data : {
						id : id
					},
					success : function(response) {
						$("#form-content").html(response);
					}
				});
			

		});
		$("#dataTables-example").on('click', '#updateContent', function() {
			var currentRow = $(this).closest("tr");
			var idEdit = currentRow.find("td:eq(0)").text();
			$.get({
				url : "editContent",
				data : {
					idEdit : idEdit
				},
				success : function(response) {
					/*$("#contentId1").css('display', 'none');
					$("#contentId1").text(idEdit);*/
					$("#form-content").html(response);
				}
			});
		});
		
	});