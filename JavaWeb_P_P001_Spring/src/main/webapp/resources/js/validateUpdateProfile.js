    
$(document).ready(function() {
$("#profile-edit").validate({
        rules: {
            firstName: {
                required: true,
                rangelength: [10, 30]
            },
            lastName: {
                required: true,
                rangelength: [10, 30]
            },
            phone: {
                required: true,
                number: true,
                rangelength: [9, 13]
            },
            description: {
                maxlength: 200
            }
        }
    });
});