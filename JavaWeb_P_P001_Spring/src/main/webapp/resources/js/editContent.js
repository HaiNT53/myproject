$(document).ready(function() {
	$('#btn-edit').click(function() {
		var id = $('#contentId').val();
		var title = $('#title').val();
		var brief = $('#brief').val();
		var content = $('#content').val();
		var contentData = {};
		contentData['contentId'] = id;
		contentData['title'] = title;
		contentData['brief'] = brief;
		contentData['content'] = content;
		if ($('#input_info').valid()) {
			$.ajax({
				url : 'editContent',
				type : 'POST',
				data : contentData,
				success : function(data) {
					
					$('#form-content').html(data);
				}
			});
		}

	});
});