<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/register.css">
</head>
<body>
	<div class="register">

		<div class="register_title">
			<h3>Register</h3>
			<p id="error">${param['error']}</p>
			<p id="done">${param['done']}</p>

		</div>
		<form:form class="formlogin" id="formregister" action="memberRegister"
			method="post" name="frm-register" modelAttribute="member">
			<div class="form-group">
				<input type="text" class="form-control" id="userName"
					name="userName" placeholder="User name">
			</div>
			<div class="form-group">
				<input type="email" class="form-control" id="email" name="email"
					placeholder="Email address">
			</div>

			<div class="form-group">
				<input type="password" class="form-control" id="password"
					name="password" placeholder="Password">
			</div>
			<div class="form-group">
				<input type="Password" class="form-control" id="confirmPassword"
					name="confirmPassword" placeholder="Re Password">
			</div>
			<div>
				<button type="submit" name="register" id="btn-register">Register</button>
			</div>
			<div class="form-group">
				<a href="login" > Click here to Login </a>
			</div>

		</form:form>
	</div>
	<%-- <div class="register">

		<div class="register_title">
			<h3>Register</h3>
			<c:if test="${param['error']}">Incorrect Username or Password</c:if>

		</div>
		<form:form id="register_form" action="userRegister" method="POST"
			modelAttribute="member">

			<form:input path="userName" type="text" class="form-control"
				id="userName" name="userName" placeholder="User-name" />
			<form:input path="email" type="email" class="form-control" id="email"
				name="email" placeholder="E-mail" />
			<form:input path="password" type="password" class="form-control"
				id="password" name="password" placeholder="Password" />
			<form:input path="password" type="password" class="form-control"
				id="repassword" name="repassword" placeholder="Re-Password" />
			<form:button name="register" id="btn-register">Register</form:button>
		</form:form>
		<a class="login_link" href="<%=request.getContextPath()%>/login">
			Click here to Login</a>
	</div> --%>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/validate-login.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script
		src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

</body>
</html>