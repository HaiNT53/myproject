<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/menu.css">
<div class="info-head">
	<h2>Edit Content</h2>
</div>
<div class="edit_profile">
	<div class="title">
		<h6>Edit Content Elements</h6>
	</div>
	<div class="input_info_edit">
		<form:form action="editContent" method="POST" name="frm-editContent" id="input_info" modelAttribute ="content">
			<p id="done">${param['message']}</p>
			<p id="error">${error}</p>
			<div class="form-group">
				<label for="title">Title</label> <form:input type="text" path="title"
					class="form-control " id="title" name="title"
					placeholder="Enter the title" value=""/>
			</div>
			<div class="form-group">
				<label for="brief">Brief</label>
				<form:textarea name="brief" path="brief" id="brief" class="form-control" rows="2"
					placeholder="Write something here"></form:textarea>
			</div>
			<div class="form-group">
				<label for="content">Content</label>
				<form:textarea name="content" path="content" id="content" class="form-control" rows="4"
					placeholder="Write something here"></form:textarea>
			</div>
			<form:input type="hidden"  id="contentId" path="contentId" />
			<button id="btn-edit" type="button">Submit</button>
			<button type="reset">Reset</button>
		</form:form>
	</div>
</div>

<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/js/validateAddContent.js"></script>
	
		<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/editContent.js"></script>
