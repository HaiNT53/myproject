
package com.fa.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Content")
public class Content  {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "contentId")
	private Integer contentId;
	@Column(name = "title")
	@Size(max = 200, min = 10, message = "Your title must be at least 10 characters long")
	private String title;
	@Column(name = "brief")
	@Size(max = 150, min = 10, message = "Your brief must be at least 10 characters long")
	private String brief;
	@Column(name = "content")
	@Size(max = 500, min = 10, message = "Your content must be at least 10 characters long")
	private String content;
	@Column(name = "createDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	@Column(name = "updateDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@JoinColumn(name = "authorId", referencedColumnName = "memberId")
	@ManyToOne(fetch = FetchType.LAZY)
	private Member authorId;

	public Content() {
	}

	public Content(Integer contentId) {
		this.contentId = contentId;
	}

	public Content(String title, String brief, String content, Member authorId, Date createDate) {
		super();
		this.title = title;
		this.brief = brief;
		this.content = content;
		this.createDate = createDate;
		this.authorId = authorId;
	}

	public Content(String title, String brief, String content, Date createDate, Date updateDate, Member authorId) {
		super();
		this.title = title;
		this.brief = brief;
		this.content = content;
		this.createDate = createDate;
		this.updateDate = updateDate;
		this.authorId = authorId;
	}

	public Content(Integer contentId, String title, String brief, String content, Date createDate, Date updateDate,
			Member authorId) {
		super();
		this.contentId = contentId;
		this.title = title;
		this.brief = brief;
		this.content = content;
		this.createDate = createDate;
		this.updateDate = updateDate;

		this.authorId = authorId;
	}

	public Integer getContentId() {
		return contentId;
	}

	public void setContentId(Integer contentId) {
		this.contentId = contentId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Member getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Member authorId) {
		this.authorId = authorId;
	}



}
