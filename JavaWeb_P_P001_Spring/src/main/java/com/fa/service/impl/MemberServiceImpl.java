package com.fa.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fa.dao.MemberDao;
import com.fa.entities.Member;
import com.fa.service.MemberService;

@Service
public class MemberServiceImpl implements MemberService {
	
	@Autowired
	private MemberDao memberDao;

	@Override
	@Transactional
	public Member login(String email, String password) {
		return memberDao.login(email, password);
	}

	@Override
	@Transactional
	public boolean addMember(Member member) {
		return memberDao.addMember(member);
	}

	@Override
	@Transactional
	public Member checkEmail(String email) {
		return memberDao.checkEmail(email);
	}

	@Override
	@Transactional
	public void updateMember(Member member) {
		memberDao.updateMember(member);
	}
	
}
