package com.fa.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fa.dao.ContentDao;
import com.fa.entities.Content;
import com.fa.service.ContentService;

@Service
public class ContentServiceImpl implements ContentService {
	
	@Autowired
	private ContentDao contentDao;

	@Override
	@Transactional
	public List<Content> getAllContent(int memberId) {
		return contentDao.getAllContent(memberId);
	}

	@Override
	@Transactional
	public boolean addContent(Content content) {
		return contentDao.addContent(content);
	}

	@Override
	@Transactional
	public void updateContent(Content content) {
		contentDao.updateContent(content);
	}

	@Override
	@Transactional
	public boolean deleteContent(Content content) {
		return contentDao.deleteContent(content);
	}

	@Override
	@Transactional
	public Content getContentById(Integer contentId) {
		return contentDao.getContentById(contentId);
	}
	
}
