package com.fa.controller;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.fa.entities.Member;
import com.fa.service.MemberService;

@Controller
public class MemberController {

	@Autowired
	private MemberService memberService;

	@RequestMapping(value = "login")
	public String showLogin(Model model) {
		model.addAttribute("member", new Member());
		return "login";
	}

	@RequestMapping(value = "index")
	public String login(@ModelAttribute("member") Member member, Model model, HttpSession session) {
		Member memberLogin = memberService.login(member.getEmail(), member.getPassword());
		if (memberLogin != null) {
			model.addAttribute("member", memberLogin);
			session.setAttribute("memberLogin", memberLogin);
			return "index";
		} else {
			model.addAttribute("error", "login false");
			return "redirect:login";
		}
	}

	@RequestMapping(value = "register")
	public String showRegister(Model model) {
		model.addAttribute("member", new Member());
		return "register";
	}

	@RequestMapping(value = "memberRegister")
	public String showRegister(@ModelAttribute("member") Member member, Model model) {
		if (memberService.checkEmail(member.getEmail()) == null) {
			member.setCreateDate(new Date());
			memberService.addMember(member);
			model.addAttribute("member", member);
			model.addAttribute("done", "register success!");
			return "redirect:register";
		} else {
			model.addAttribute("error", "Email already exists!");
			return "redirect:register";
		}

	}

	@RequestMapping("editProfile")
	public String showEditProfile(Model model) {
		model.addAttribute("member", new Member());
		return "profile-edit";
	}

	@RequestMapping(value = "edit", method = RequestMethod.POST)
	public String editProfile(@ModelAttribute("member") Member member,
			@SessionAttribute("memberLogin") Member memberLogin, Model model) {		
			memberLogin.setFirstName(member.getFirstName());
			memberLogin.setLastName(member.getLastName());
			memberLogin.setPhone(member.getPhone());
			memberLogin.setDescription(member.getDescription());
			memberLogin.setUpdateDate(new Date());
			memberService.updateMember(memberLogin);
			model.addAttribute("message", "Edit successfully!");
	
			return "index";
	}

}
